package com.univ.web;

import com.univ.jms.Sender;
import com.univ.models.Book;
import com.univ.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/rest/books")
public class BookController {

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private Sender book_sender;
    @Value(value = "BOOK.PROCESSING.D")
    private String book_destination;

    @GetMapping(value = "/books/all")
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @PostMapping(value = "/books/add")
    public Book addBook(@RequestBody Book book) {
        bookRepository.save(book);
        book_sender.send(book_destination, book);
        return book;
    }

    @PutMapping(value = "/books/{bookId}")
    public Book updateBook(@PathVariable("bookId") int bookId, @RequestBody Book book) {
        book.setBookId(bookId);
        bookRepository.save(book);
        return book;
    }

    @DeleteMapping(value = "/books/{bookId}")
    public List<Book> removeBook(@PathVariable("bookId") int bookId) {
        bookRepository.delete(bookId);
        return bookRepository.findAll();
    }
}
