package com.univ.web;

import com.univ.jms.Sender;
import com.univ.models.Rent;
import com.univ.repositories.RentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/rest/rents")
public class RentController {

    @Autowired
    private RentRepository rentRepository;
    @Autowired
    private Sender sender;
    @Value(value = "RENT.PROCESSING.D")
    private String destination;

    @GetMapping(value = "/all")
    public List<Rent> getAll(@PathVariable("id") int id) {
        return rentRepository.findByPersonId(id);
    }

    @PostMapping(value = "/add")
    public Rent addRent(@PathVariable("id") int id, @RequestBody Rent rent) {
        rentRepository.save(rent);
        sender.send(destination, rent);
        return rent;
    }

    @PutMapping(value = "/{rentId}")
    public Rent updateRent(@PathVariable("id") int id, @PathVariable("rentId") int rentId, @RequestBody Rent rent){
        rent.setRentId(rentId);
        rentRepository.save(rent);
        return rent;
    }

    @DeleteMapping(value = "/{rentId}")
    public List<Rent> removeRent(@PathVariable("id") int id, @PathVariable("rentId") int rentId){
        rentRepository.delete(rentId);
        return rentRepository.findByPersonId(id);
    }
}
