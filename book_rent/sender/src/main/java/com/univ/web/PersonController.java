package com.univ.web;

import com.univ.jms.Sender;
import com.univ.models.Person;
import com.univ.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/rest")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private Sender sender;
    @Value(value = "PERSON.PROCESSING.D")
    private String destination;

    @GetMapping(value = "/login")
    public List<Person> login() {
        List<Person> result = personRepository.findAll();
        return result;
    }

    @GetMapping(value = "/registration")
    public Person registration(@RequestBody Person person) {
        personRepository.save(person);
        sender.send(destination, person);
        return person;
    }
}
