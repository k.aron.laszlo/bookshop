package com.univ.models;

import javax.persistence.*;
import java.util.Date;

public class Rent {

    @Id
    @GeneratedValue
    private int rentId;
    @Temporal(TemporalType.DATE)
    private Date beginRent;
    @Temporal(TemporalType.DATE)
    private Date endRent;
    private int numberOfDays = 0;
    @ManyToOne
    @JoinColumn(name="PERSON_ID")
    private Person person;
    @ManyToOne
    @JoinColumn(name="BOOK_ID")
    private Book book;

    public Rent() {}

    public int getRentId() {
        return rentId;
    }

    public void setRentId(int rentId) {
        this.rentId = rentId;
    }

    public Date getBeginRent() {
        return beginRent;
    }

    public void setBeginRent(Date beginRent) {
        this.beginRent = beginRent;
    }

    public Date getEndRent() {
        return endRent;
    }

    public void setEndRent(Date endRent) {
        this.endRent = endRent;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
