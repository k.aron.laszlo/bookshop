package com.univ.models;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class Person {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String email;
    @Temporal(TemporalType.DATE)
    private Date date;
    @OneToMany(mappedBy="person", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Rent> rents = new ArrayList<Rent>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Rent> getRents() {
        return rents;
    }

    public void setRents(List<Rent> rents) {
        this.rents = rents;
    }
}
