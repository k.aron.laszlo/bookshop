package com.univ.models;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Book {

    @Id
    @GeneratedValue
    private int bookId;
    private String title;
    private String author;
    private String chategory;
    private String prolog;
    private int content;
    @OneToMany(mappedBy="book", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Rent> rents = new ArrayList<Rent>();

    public Book() {}

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getChategory() {
        return chategory;
    }

    public void setChategory(String chategory) {
        this.chategory = chategory;
    }

    public String getProlog() {
        return prolog;
    }

    public void setProlog(String prolog) {
        this.prolog = prolog;
    }

    public int getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }

}
