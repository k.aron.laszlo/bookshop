package com.univ.repositories;

import com.univ.models.Rent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RentRepository extends JpaRepository<Rent, Integer> {

    public List<Rent> findByPersonId(int personId);
}
