package com.univ.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class BookReciever {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookReciever.class);

    @JmsListener(destination = "BOOK.PROCESSING.D")
    public void recieve(String message) {
        LOGGER.info("recieved message = '{}'", message);
    }
}
