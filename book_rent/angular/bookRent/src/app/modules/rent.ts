import {Person} from './person';
import {Book} from './book';

export class Rent {
  rentId: number;
  beginRent: Date;
  endRent: Date;
  numberOfDays: number;
  finalPrice: number;
  person: Person;
  book: Book;
}
