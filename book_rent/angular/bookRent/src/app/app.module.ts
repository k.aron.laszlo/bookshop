import { BrowserModule } from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SortByPipe } from './pipes/sort-by.pipe';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {BookService} from './service/book.service';
import {RentService} from './service/rent.service';
import {PersonService} from './service/person.service';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RentListComponent } from './components/rent-list/rent-list.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { AddRentComponent } from './components/add-rent/add-rent.component';
import { RegistrationComponent } from './components/registration/registration.component';

@NgModule({
  declarations: [
    AppComponent,
    SortByPipe,
    HomeComponent,
    NavbarComponent,
    RentListComponent,
    BookListComponent,
    AddBookComponent,
    AddRentComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    HttpModule,
    FormsModule,
    AppRoutingModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [PersonService, BookService, RentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
