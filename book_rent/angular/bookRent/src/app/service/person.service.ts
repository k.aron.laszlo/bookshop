import { Injectable } from '@angular/core';
import {Person} from '../modules/person';
import 'rxjs/add/operator/map';

@Injectable()
export class PersonService {

  constructor(private http) { }

  registration(person: Person) {
    return this.http.post('http://localhost:8080/rest/registration', person).map(res => res.json());
  }
}
