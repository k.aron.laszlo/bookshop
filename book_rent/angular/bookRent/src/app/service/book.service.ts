import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Book} from '../modules/book';

@Injectable()
export class BookService {

  constructor(private http: Http) { }

  getBook() {
    return this.http.get('http://localhost:8080/rest/books/all').map(res => res.json());
  }

  addBook(book: Book) {
    return this.http.post('http://localhost:8080/rest/books/add', book).map(res => res.json());
  }

  updateCar(book: Book) {
    return this.http.put('http://localhost:8080/rest/vehicules/cars/' + book.bookId, book).map(res => res.json());
  }

  deleteCar(bookId) {
    return this.http.delete('http://localhost:8080/rest/vehicules/cars/' + bookId).map(res => res.json());
  }
}
