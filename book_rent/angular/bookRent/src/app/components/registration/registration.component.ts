import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../service/person.service';
import { Person } from '../../modules/person';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit {

  person = new Person();
  constructor(private personService: PersonService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.personService.registration(this.person).subscribe(res => {
      window.location.href = '/persons';
    });
  }

}
