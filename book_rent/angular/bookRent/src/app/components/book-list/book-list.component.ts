import {Component, OnInit, ViewChild} from '@angular/core';
import { BookService } from '../../service/book.service';
import { ModalDirective } from '../../../../node_modules/angular-bootstrap-md';
import { NgForm } from '@angular/forms';
import { Book } from '../../modules/Book';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.sass']
})
export class BookListComponent implements OnInit {

  @ViewChild('updateBookModal') updateBookModal: ModalDirective;
  @ViewChild('deleteBookConf') deleteBookConf: ModalDirective;
  @ViewChild('bookBadInput') bookBadInput: ModalDirective;
  @ViewChild('updateBookForm') updateBookForm: NgForm;
  errorMessage: String = 'Please fill inputs correctly.';
  books = new Array<Book>();
  book = new Book();
  sortDirectionBook: String = 'asc';
  sortFieldBook: String = 'model';
  sortFieldsBook: Array<String> = [
    'title',
    'author',
    'chategory',
    'prolog',
    'content',
    'price',
  ];

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.bookService.getBook().subscribe(books => {
      this.books = books;
    });
  }

  onDeleteClickBook(bookId) {
    this.deleteBookConf.show();
    this.book.bookId = bookId;
  }

  onSubmitBook() {
    let cpt;
    this.book.title = this.updateBookForm.value.model;
    this.book.author = this.updateBookForm.value.author;
    this.book.chategory = this.updateBookForm.value.chategory;
    this.book.prolog = this.updateBookForm.value.prolog;
    this.book.content = this.updateBookForm.value.content;
    this.book.price = this.updateBookForm.value.price;
    this.bookService.updateCar(this.book).subscribe(res => {
      for (let i = 0; i < this.books.length; i++) {
        if (this.books[i].vehiculeId === this.books.vehiculeId) {
          this.books.splice(i, 1);
          cpt = i;
        }
      }
      this.books.splice(cpt, 0, this.book);
      this.updateBookModal.hide();
    });
  }

  onEditClickBook(book: Book) {
    this.book = book;
    this.updateBookModal.show();
  }

  onProceedBook() {
    this.bookService.deleteCar(this.book.bookId).subscribe(res => {
      for (let i = 0; i < this.books.length; i++) {
        if (this.books[i].vehiculeId === this.book.bookId) {
          this.books.splice(i, 1);
        }
      }
      this.deleteBookConf.hide();
    });
  }

  onAscBook() {
    this.changeStyleBookAsc();
    this.sortDirectionBook = 'asc';
  }

  onDescBook() {
    this.changeStyleBookDesc();
    this.sortDirectionBook = 'desc';
  }


  changeStyleBookAsc() {
    const button = document.getElementById('ascBook');
    const button2 = document.getElementById('descBook');
    if (button.click) {
      button.className = 'btn btn-light-green btn-sm';
      button.blur();
      button2.className = 'btn btn-mdb btn-sm';
    }
  }

  changeStyleBookDesc() {
    const button = document.getElementById('ascBook');
    const button2 = document.getElementById('descBook');
    if (button2.click) {
      button2.className = 'btn btn-light-green btn-sm';
      button2.blur();
      button.className = 'btn btn-mdb btn-sm';
    }
  }

}
