import { Component, OnInit, ViewChild } from '@angular/core';
import { BookService } from '../../service/book.service';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'angular-bootstrap-md/modals/modal.directive';
import { Book } from '../../modules/book';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.sass']
})
export class AddBookComponent implements OnInit {

  @ViewChild('carBadInput') carBadInput: ModalDirective;
  @ViewChild('addBookForm') addCarForm: NgForm;
  errorMessage: String = 'There was an error filling up the form. Please try again.';
  book = new Book();
  choice: Boolean = false;

  constructor(private bookService: BookService) { }

  ngOnInit() {}

  onSubmitBook() {
    this.bookService.addBook(this.book).subscribe(res => {
      window.location.href = '/books';
    });
  }

  toggleBook() {
    this.choice = false;
  }

}
