import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'angular-bootstrap-md/modals/modal.directive';
import { Rent } from '../../modules/rent';
import {RentService} from '../../service/rent.service';
import {BookService} from '../../service/book.service';
import {Book} from '../../modules/book';

@Component({
  selector: 'app-add-rent',
  templateUrl: './add-rent.component.html',
  styleUrls: ['./add-rent.component.sass']
})
export class AddRentComponent implements OnInit {

  @ViewChild('addRentForm') addRentForm: NgForm;
  @ViewChild('badInput') badInput: ModalDirective;
  errorMessage: String = 'There was an error when filling up the form. Please try again.';
  rent = new Rent();
  receivedPerson;
  bookList = new Array<Book>();

  constructor(private rentService: RentService, private bookService: BookService) { }

  ngOnInit() {
    this.receivedPerson = JSON.parse(localStorage.getItem('user'));
    this.bookService.getBook().subscribe(books => {
      for (let i = 0; i < books.length; i++) {
        this.bookList.push(books[i]);
      }
    });
  }

  onSubmit() {
    this.rent.person = this.receivedPerson;
    this.rent.numberOfDays = this.rentService.computeDays(this.rent);
    this.rent.finalPrice = this.rentService.computePrice(this.rent);
    if (this.rent.beginRent > this.rent.endRent) {
      this.errorMessage = 'You must specify valid begin and end date. Please try again.';
      this.badInput.show();

    } else {
      this.rentService.addRent(this.rent.person.id, this.rent).subscribe(res => {
        window.location.href = '/rents';
      });
    }
  }

}
